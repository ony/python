# Copyright 2015 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require pypi setup-py [ import=setuptools ]

SUMMARY="Backport of the Python stdlib linecache module"

LICENCES="PSF-2.2"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-python/pbr[python_abis:*(-)?]
    suggestion:
        (
            dev-python/fixtures[python_abis:*(-)?]
            dev-python/unittest2[python_abis:*(-)?]
        ) [[
            *group-name = [ test ]
            *description = [ Dependencies for recommended tests, suggested to break cycles ]
        ]]
"

prepare_one_multibuild() {
    # workaround for python 2.7
    # https://github.com/testing-cabal/linecache2/issues/2
    if has ${MULTIBUILD_TARGET} 2.7; then
        edo rm -r linecache2/tests
    fi

    setup-py_prepare_one_multibuild
}

test_one_multibuild() {
    # avoid cycles
    if has_version dev-python/fixtures[python_abis:$(python_get_abi)] &&
       has_version dev-python/unittest2[python_abis:$(python_get_abi)]; then
        edo ${PYTHON} -m unittest2
    else
        ewarn "fixtures and/or unittest2 not yet installed, skipping tests"
    fi
}

