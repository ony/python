# Copyright 2008-2017 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'beautifulsoup-3.0.7.ebuild' from Gentoo, which is:
#     Copyright 1999-2008 Gentoo Foundation

require pypi
require setup-py [ import=distutils blacklist=3 ]

SUMMARY="HTML/XML parser for quick-turnaround applications like screen-scraping."
DESCRIPTION="
Beautiful Soup parses anything you give it, and does the tree traversal stuff for
you. You can tell it Find-all-the-links, or Find-all-the-links-of-class-externalLink,
or Find-all-the-links-whose-urls match-foo.com and much more.
Beautiful Soup won't choke if you give it bad markup. It yields a parse tree that
makes approximately as much sense as your original document. This is usually good
enough to collect the data you need and run away.
Beautiful Soup provides a few simple methods and Pythonic idioms for navigating,
searching, and modifying a parse tree: a toolkit for dissecting a document and
extracting what you need. You don't have to create a custom parser for each
application.
Beautiful Soup automatically converts incoming documents to Unicode and outgoing
documents to UTF-8. You don't have to think about encodings, unless the document
doesn't specify an encoding and Beautiful Soup can't autodetect one. Then you
just have to specify the original encoding.
"
HOMEPAGE="http://www.crummy.com/software/${PN}/"
DOWNLOADS="http://www.crummy.com/software/${PN}/download/$(ever major).x/${PNV}.tar.gz"

#BUGS_TO="philantrop@exherbo.org"

UPSTREAM_CHANGELOG="http://www.crummy.com/software/${PN}/CHANGELOG.html"
UPSTREAM_DOCUMENTATION="http://www.crummy.com/software/${PN}/documentation.html [[ lang = en ]]"

LICENCES="BSD-3"
SLOT="0"
MYOPTIONS=""
DEPENDENCIES=""

test_one_multibuild() {
    PYTHONPATH="." "${PYTHON}" BeautifulSoupTests.py || die "tests failed"
}

