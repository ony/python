# Copyright 2017 Rasmus Thomsen <Rasmus.thomsen@protonmail.com>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'twisted-17.5.0-r1.ebuild' from Gentoo, which is:
#    Copyright 1999-2017 Gentoo Foundation

require setup-py [ blacklist=none import=setuptools has_bin=true ]
require flag-o-matic
require utf8-locale

SUMMARY="Twisted is an event-driven networking engine"
DESCRIPTION="
Twisted is an event-based framework for internet applications, supporting Python 2.7 and Python
3.3+. It includes modules for many different purposes, including the following: twisted.web: HTTP
clients and servers, HTML templating, and a WSGI server twisted.conch: SSHv2 and Telnet clients and
servers and terminal emulators twisted.words: Clients and servers for IRC, XMPP, and other IM
protocols twisted.mail: IMAPv4, POP3, SMTP clients and servers twisted.positioning: Tools for
communicating with NMEA-compatible GPS receivers twisted.names: DNS client and tools for making
your own DNS servers twisted.trial: A unit testing framework that integrates well with
Twisted-based code
"
HOMEPAGE="https://twistedmatrix.com"
DOWNLOADS="${HOMEPAGE}/Releases/${PN}/$(ever range 1-2 ${PV})/${PNV}.tar.bz2"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

#TODO: python2.7 soap, http2
DEPENDENCIES="
    build+run:
        dev-python/attrs[>=17.4.0][python_abis:*(-)?]
        dev-python/Automat[>=0.3.0][python_abis:*(-)?]
        dev-python/constantly[>=15.1.0][python_abis:*(-)?]
        dev-python/hyperlink[>=17.1.1][python_abis:*(-)?]
        dev-python/idna[>=0.6][python_abis:*(-)?]
        dev-python/incremental[>=16.10.1][python_abis:*(-)?]
        dev-python/PyHamcrest[>=1.9.0][python_abis:*(-)?]
        dev-python/pyopenssl[>=16.0.0][python_abis:*(-)?]
        dev-python/service_identity[python_abis:*(-)?]
        dev-python/zopeinterface[>=4.4.2][python_abis:*(-)?]
        (
            !net-twisted/TwistedConch
            !net-twisted/TwistedCore
            !net-twisted/TwistedLore
            !net-twisted/TwistedMail
            !net-twisted/TwistedNames
            !net-twisted/TwistedNews
            !net-twisted/TwistedRunner
            !net-twisted/TwistedWeb
            !net-twisted/TwistedWords
        ) [[
            *description = [ Old Twisted packages, now provided by Twisted ]
            *resolution = uninstall-blocked-before
        ]]
    test:
        dev-python/appdirs[>=1.4.0][python_abis:*(-)?]
        dev-python/cryptography[>=1.5][python_abis:*(-)?]
        dev-python/gmpy2[python_abis:*(-)?]
        dev-python/idna[>=0.6][python_abis:*(-)?]
        dev-python/pyasn1[python_abis:*(-)?]
        dev-python/pyopenssl[>=16.0.0][python_abis:*(-)?]
        dev-python/pyserial[python_abis:*(-)?]
        dev-python/service_identity[python_abis:*(-)?]
    suggestion:
        (
            dev-python/appdirs[>=1.4.0][python_abis:*(-)?]
            dev-python/cryptography[>=1.5][python_abis:*(-)?]
            dev-python/pyasn1[python_abis:*(-)?]
        ) [[
            *description = [ Required for Twisted's SSHv2 implementation ]
        ]]
        dev-python/pyserial[>=3.0][python_abis:*(-)?] [[ description = [
            Use pySerial to provide support for an asynchronous serial port transport ] ]]
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/twisted-test-override.patch
    "${FILES}"/twisted-disable-h2-tests.patch  # fails without h2 library
    "${FILES}"/twisted-disable-openssh-tests.patch
    "${FILES}"/twisted-disable-openssh-server-test.patch
    "${FILES}"/twisted-remove-service-identify-recognition-test.patch
    "${FILES}"/twisted-utf8-overrides.patch
)

pkg_setup() {
    require_utf8_locale  # required for tests
}

prepare_one_multibuild() {
    # Remove since this is an upstream distribution test for making releases
    edo rm src/twisted/python/test/test_release.py

    # failing tests (at least on CI)
    edo rm src/twisted/test/test_main.py
    # last checked: 18.7.0
    edo rm src/twisted/conch/test/test_ckeygen.py

    # Remove patches which fail with libressl
    option providers:libressl && expatch -p1 "${FILES}"/twisted-remove-openssl-specific-tests.patch


    # Testing with pytest doesn't work, so we'll have to do it with Twisted's tool
    edo cp "${FILES}"/trial trial

    setup-py_prepare_one_multibuild
}

compile_one_multibuild() {
    # see http://twistedmatrix.com/trac/ticket/5701
    add-flag -fno-strict-aliasing

    setup-py_compile_one_multibuild
}

test_one_multibuild() {
    PYWORK="${WORKBASE}/${MULTIBUILD_CLASS}/${MULTIBUILD_TARGET}/${PNV}"
    local item
    local whitelist=(
        "--connect inet6:::1@1024-65535"
        "--connect inet:0.0.0.0@0-65535"
        "--connect inet:127.0.0.1@024-65535"
        "--connect inet:127.0.0.1@69"
        "--connect inet:127.0.0.1@80"
        "--connect unix:*"
        "inet:0.0.0.0@0-65535"
        "unix-abstract:*"
        "unix:${PYWORK}/_trial_temp/*"  # a few tests create files with random names
        "unix:${PYWORK}/_trial_temp/twisted.internet.test.test_unix/*/*/*/temp"
        "unix:${PYWORK}/_trial_temp/twisted.test.test_unix/*/*/*/temp"
        "unix:${PYWORK}/_trial_temp/twisted.web.test.test_tap/*/*/*/temp"
        "unix:${TEMP%/}/tmp*"
    )

    for item in "${whitelist[@]}"; do
        esandbox allow_net ${item}
    done

    # Disable a few tests which don't play nice with sydbox
    export TEST_OVERRIDE=1
    export UTF8_OVERRIDES=1

    # Trial actually has a --jobs option, but some hundred of Twisted's
    # test fail when parallelized.
    # Disable gobject because Glib2Reactor tests fail with "too many open files".
    PYTHONPATH="$(ls -d build/lib*)" edo ${PYTHON} -B trial --without-module=gobject twisted

    export UTF8_OVERRIDES=0
    PYTHONPATH="$(ls -d build/lib*)" edo ${PYTHON} -B trial twisted.test.test_twistd.DaemonizeTests
    PYTHONPATH="$(ls -d build/lib*)" edo ${PYTHON} -B trial twisted.test.test_reflect.SafeStrTests

    for item in "${whitelist[@]}"; do
        esandbox disallow_net ${item}
    done
}

